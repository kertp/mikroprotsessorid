/*
 * main.c
 *
 * Created: 14.05.2013 10:22:03
 *  Author: Kert
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>

#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))

struct mouse{
    uint8_t first_byte;
    uint8_t second_byte;
    uint8_t third_byte;
};

void checkButton( struct mouse *);
void checkMovement( struct mouse *);
void resetBits( struct mouse *);
void send( struct mouse *);

int main(void)
{
    // Loon uue structi
    struct mouse *mouse;
    mouse = (struct mouse *)malloc(sizeof(struct mouse));
    mouse->first_byte = 0b1000000;
    // mouse->second_byte = 0b0000011;
    // mouse->third_byte =0b1111111;
    UBRR1 = 103;
    UCSR1B = (1 << TXEN1);
    UCSR1C = 1 << UCSZ11;
    
    // PORTA & PORTF
    DDRA = 0b11111111;
    DDRF = 0b11011111;
    PORTF = 0b11111000;
    uint8_t vajutus = 0b10000000;
    MCUCR = vajutus;
    MCUCR = vajutus;
    while(1)
    {
        PORTA = PINF;
        checkButton( mouse );
        // checkMovement( mouse );
        send( mouse );
        resetBits( mouse );
    }
}

// void checkButton( struct mouse mouse )
void checkButton( struct mouse *mouse ){
    // Kontrollib, kas hiir liigub
    checkMovement( mouse );
    // Nupuvajutus
    if(0 == CHECKBIT(PINF,6)){
        // PORTA = PINF;
        // MIKS??
        mouse->first_byte |= (1 << 6) | (1 << 5);
        // mouse->first_byte |= (1 << 5);
        // mouse->first_byte = 0b1100000;
        // mouse->second_byte = 0b0000000;
        // mouse->third_byte = 0b0000000;
        
        send( mouse );
        resetBits( mouse );
        while(0 == CHECKBIT(PINF,6));
        // MIKS???
        mouse->first_byte &= (0 << 5);
        mouse->first_byte |= (1 << 6);
        // mouse->first_byte = 0b1000000;
        // mouse->second_byte = 0b0000000;
        // mouse->third_byte = 0b0000000;
        // send( mouse );
    }
    // Taastab bittid algseisu
}

void checkMovement(struct mouse *mouse){
    // alla
    if( 0 == CHECKBIT(PINF, 4) ){ mouse->third_byte = 20;}
    // üles
    else if( 0 == CHECKBIT(PINF, 7)){
        mouse->third_byte = 31;
        mouse->first_byte |= (1 << 6) | (1 << 3) | (1 << 2);
    }
    //paremale
    else if( 0 == CHECKBIT(PINF,3)){
        mouse->second_byte = 0b0000011;
        mouse->third_byte =0b1111111;
        mouse->first_byte |= (1 << 6) | ( 1 << 3) | (1 << 1);
    }
    // mouse->third_byte = mov; 
    // send( mouse );
    // mouse->third_byte = 0;
}

void resetBits( struct mouse *mouse){
    mouse->second_byte = 0;
    mouse->third_byte = 0;
    mouse->first_byte &= (0 << 3) | ( 0 << 2);
    mouse->first_byte |= (1 << 6);
}

void send( struct mouse *mouse ){
    // PORTA = mouse->first_byte;
    // PORTA = mouse->second_byte;
    while(CHECKBIT(UCSR1A, 5) == 0);
    UDR1 = mouse->first_byte;
    while(CHECKBIT(UCSR1A, 5) == 0);
    UDR1 = mouse->second_byte;
    while(CHECKBIT(UCSR1A, 5) == 0);
    UDR1 = mouse->third_byte;
}
