AS=avr-as
GCC=avr-gcc
OBJCOPY=avr-objcopy
OBJDUMP=avr-objdump
CPP=avr-cpp
DFUPROG=dfu-programmer
CPU=at90usb647

all: main

main: main.c
	$(GCC) -o main.elf -mmcu=at90usb647 main.c -O2 -lc -lm
	$(OBJCOPY) -O ihex main.elf main.hex -j .text
	$(OBJDUMP) -s -D -M intel -m avr:6  main.elf > main.asm
	$(DFUPROG) $(CPU) erase
	$(DFUPROG) $(CPU) flash main.hex
	$(DFUPROG) $(CPU) start

clean:
	rm main.elf main.hex main.asm 

program:
	$(DFUPROG) $(CPU) erase
	$(DFUPROG) $(CPU) flash main.hex
	$(DFUPROG) $(CPU) start
